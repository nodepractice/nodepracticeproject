FROM node

# henceforth use /src as current / working directory inside the image
WORKDIR /src

# copy everything from current directory to the working directory inside image
# this command will copy everything from current directory on local machine 
# to the /src of the image
COPY . .

# since our server is expected to run on port 3000
EXPOSE 3000

# start the server when a container starts from this image
# ths command will execute only when a container starts and will run inside 
# the container
CMD node server.js